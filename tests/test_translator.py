import contextlib
import io
import logging
import os
import tempfile
import traceback

import csa_lab3.machine.main
import csa_lab3.translator.main
import csa_lab3.translator.parser as parse
import csa_lab3.translator.tokenizer as tokens
import pytest
from csa_lab3.translator.common import ParseError


@pytest.mark.golden_test("../golden/*.yml")
def test_tokenizer(golden):
    with contextlib.redirect_stdout(io.StringIO()) as stdout:
        with io.StringIO(golden["in_source"]) as f:
            tokenizer = tokens.Tokenizer(f)
            token = tokenizer.get_next_token()
            while token:
                print(f"{token.token_type.name}: {token.value}")
                token = tokenizer.get_next_token()
    assert stdout.getvalue() == golden.out["out_tokens"]


@pytest.mark.golden_test("../golden/*.yml")
def test_parser(golden):
    with contextlib.redirect_stdout(io.StringIO()) as stdout:
        try:
            with io.StringIO(golden["in_source"]) as f:
                parser = parse.Parser(f)
                stmt = parser.get_next_stmt()
                while stmt:
                    print(stmt)
                    stmt = parser.get_next_stmt()
        except ParseError as ex:
            print(ex)
            traceback.print_exc(file=stdout)
    assert stdout.getvalue() == golden.out["out_ast"]


@pytest.mark.golden_test("../golden/*.yml")
def test_translator_and_machine(golden, caplog):
    caplog.set_level(logging.DEBUG)
    with tempfile.TemporaryDirectory() as tmpdirname:
        # Готовим имена файлов для входных и выходных данных.
        source = os.path.join(tmpdirname, "source.cmm")
        input_stream = os.path.join(tmpdirname, "input.txt")
        target = os.path.join(tmpdirname, "target.o")

        # Записываем входные данные в файлы. Данные берутся из теста.
        with open(source, "w", encoding="utf-8") as file:
            file.write(golden["in_source"])
        with open(input_stream, "w", encoding="utf-8") as file:
            if golden["in_stdin"] is not None:
                file.write(str(golden["in_stdin"]) + "\n")

        # Запускаем транслятор и собираем весь стандартный вывод в переменную
        # stdout
        with contextlib.redirect_stdout(io.StringIO()) as stdout:
            csa_lab3.translator.main.main(source, target, True)
            print("============================================================")
            csa_lab3.machine.main.main(target, input_stream)

        # Выходные данные также считываем в переменные.
        with open(target, encoding="utf-8") as file:
            code = file.read()

        # Проверяем, что ожидания соответствуют реальности.
        assert code == golden.out["out_instructions"]
        assert stdout.getvalue() == golden.out["out_stdout"]
        assert caplog.text == golden.out["out_log"]
