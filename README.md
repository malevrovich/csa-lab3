# csa-lab3

- P33092, Скрябин Иван Александрович
- `alg | acc | harv | hw | tick | struct | stream | mem | pstr | prob2 | [4]char`
- без усложнения

# Язык программирования
- Токены перечисляются в [этом enum](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/csa_lab3/translator/tokenizer.py#L75). \
Особые токены:
```
NUM_LIT     := [0-9]*
STRING_LIT  := \"[^"]*\"
CHAR_LIT    := \'.\' | \'\\n\'
VAR_LIT     := [a-bA-B][a-bA-B0-9]*
```

- Грамматика:
``` ebnf
program ::= %empty | statement | statement program

statement ::= input_expr ";" | output_stmt | assign_stmt | decl_stmt | if_stmt | while_stmt

input_expr ::= "getc" "(" ")"
literal ::= NUM_LIT | CHAR_LIT | STR_LIT | VAR_LIT
expr ::= literal |
         input_expr |
         "!" expr |
         "-" expr |
         expr "*" expr |
         expr "/" expr |
         expr "%" expr |
         expr "+" expr |
         expr "-" expr |
         expr "!=" expr |
         expr "==" expr |
         expr "<" expr |
         expr ">" expr |
         expr "|" expr |
         expr "&" expr |
         expr "[" expr "]" |
         "(" expr ")" 


output_stmt ::= "print" "(" expr ")" ";"
assign_stmt ::= expr "=" expr ";"

type ::= "int" "string" "char"
decl_stmt ::= type VAR_LIT ";" |
              "string" VAR_LIT "[" NUM_LIT "]" ";" |
              type VAR_LIT "=" expr ";"

if_stmt ::= "if" "(" expr ")" "{" program "}"
while_stmt ::= "while" "(" expr ")" "{" program "}"
```

- Семантика
    - Система типов.
        Строгая статическая типизация. Встроенные типы: 
        - `int` - числовой тип, используется всегда по значению. Может принимать значение любого числа, помещающегося в 1 слово. 
        - `char` - символьный тип, используется всегда по значению. Может принимать значение любого числа, помещающегося в 1 слово. 
        - `string` - строковый тип, работает по аналогии с `char *`, то есть используется по ссылке. Если затереть указатель, то доступ к этой памяти будет потерян. Память под строки выделяется только статически. По указателю хранятся pstr, однако невозможно узнать длину строки силами языка.

    - Каждое выражение имеет тип, который вычисляется следующим образом:
        - Каждый литерал имеет свой тип. Тип переменной задается один раз при её объявлении.
        - У каждого оператора есть список сигнатур(список типов аргументов и его возвращаемый тип).
        
    - Сигнатуры операторов. Все операторы имеют единственную сигнатуру (`int`, `int`) -> `int` за исключением:

        | Operator     | Signatures |
        | ------     | ------ |
        | NOT(**!**) | (`int`) -> `int` |
        | NEG(**-**) | (`int`) -> `int` |
        | PLUS(**+**)  | (`int`, `int`) -> `int` , (`int`, `char`) -> `char` , (`char`, `int`) -> `char` |
        | BIN_MINUS(**-**) | (`int`, `int`) -> `int` , (`char`, `char`) -> `int` |
        | NEQ(**!=**) | (`int`, `int`) -> `int` , (`char`, `char`) -> `int` |
        | EQ (**==**) | (`int`, `int`) -> `int` , (`char`, `char`) -> `int` |
        | BRACKETS(**[]**) | (`string`, `int`) -> `char` |

    - Приоритеты всех операторов стандартные, все операторы левоассоциативные. "=" - не оператор. ([приоритеты и сигнатуры задаются в этом enum](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/csa_lab3/translator/expressions.py#L13))

    - Логический тип отсутствует. Вместо него используется `int`. 0 интерпретируется как False, не 0 интерпретируется как True. if_stmt и while_stmt ожидают выражения типа `int`.

    - Реализовано некое подобие **rvalue** и **lvalue**. \
      Если выражение **lvalue** или assignable, то оно может стоять как слева, так и справа от "=", то есть оно имеет какое то значение и ему его можно присвоить. \
      Если выражение **rvalue**, то оно может стоять только справа от "=", то есть оно имеет какое-то значение, но ему его присваивать нельзя.\
      **lvalue** являются: переменная или оператор BRACKETS([]), все остальные выражения **rvalue**.

    - Области видимости всех переменных глобальные, однажды обьявленная переменная будет видна в любой строчке кода, идущей за ней.

    - Процедуры отсутствуют, output_stmt может принимать либо `char`, либо `string` и выводить их значение(строку выводит в соответствии с ее длиной указанной в префиксе).

# Организация памяти

```
Registers
+------------------------------+
| acc                          |
+------------------------------+

       Instruction memory
+------------------------------+
| 00  : program start          |
|    ...                       |
+------------------------------+

          Data memory
+-------------------------------+
| 0x10000000 : input addr(MMIO) |
| 0x10000001 : output addr(MMIO)|
|    ...                        |
| 0x20000000 : string literal 1 |
|    ...                        |
| 0x2000000F : string literal 1 |
|    ...                        |
| 0x30000000 : variable 1       |
| 0x30000001 : tmp_expr 1       |
| 0x30000002 : tmp_expr 2       |
| 0x30000003 : variable 2       |
|    ...                        |
+-------------------------------+

```
[data_memory layout](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/csa_lab3/translator/translator.py#L40)

- Размер машинного слова 32 бита. Аккумуляторная, гарвардская архитектура. Виды адресации:
    - Непосредственная(Immediate) - аргумент закодирован в команде
    - Прямая(Direct) - аргумент находится по адресу, указанному в команде
    - Косвенная(Indirect) - аргумент находится по адресу, который находится по адресу, указанному в команде.

- Численные и символьные литералы не загружаются в память, а используются как Immediate value. Во время работы транслятор записывает все строковые литералы, присуждает им адрес и аллоцирует под них место(внутри адресного пространства), по окончанию работы он добавляет в начало программы инструкции, которые загружают данные в память(имитация загрузчика ОС). [(Трансляция загрузки значения литерала в аккумулятор)](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/csa_lab3/translator/expressions.py#L263) [(Процесс аллокации места)](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/csa_lab3/translator/translator.py#L57)

- Динамическая память как таковая отсутствует. Существует лишь статическая(можно выделить место под строковый буфер), которая хранится в том же сегменте, что и данные литералов, и автоматическая(память, которая автоматически выделяется компилятором под переменные и временные значения выражений), для нее представлен отдельный сегмент. Под каждую переменную выделяется ровно одно слово.

- Автоматическая память под временные значения может быть переиспользована, поэтому существует возможность вернуть адрес в транслятор для повторного использования(свободные адреса кладутся на стек для повышения локальности).

- MMIO позволяет транслировать выражения вывода и ввода при помощи операций `ST` и `LD` и непосредственной и прямой адресацией соотвественно. Также MMIO позволяет с легкостью использовать input_stmt как часть выражения благодаря прямой адресации. Пример:
```
if ('c' == getc()) {:
  	57   LD   [IMMEDIATE] 99
  	58   CMP  [DIRECT]    268435456 <--- Адрес устройства ввода
  	59   SETE 
  	60   JZ   [IMMEDIATE] 63
```

- Процесс трансляции выражения:
    - У каждого выражения может быть: immediate value и адрес. Оба могут отсутствовать.
    - Если у выражения есть immediate value, то при трансляции будут активно этим пользоваться и не загружать его в аккумулятор в лишний раз \
      Immediate value есть у символьных и численных литералов(constexpr отсутствуют, но их не сложно добавить)
    - Если у выражения есть адрес, то этим тоже будут активно пользоваться и использовать прямую адресацию вместо загрузки значения во временную ячейку. \
      Адрес есть у строковых литералов, переменных, а также input_stmt
    - Так как все операторы левоассоциативные, то можно сделать вывод, что парсер выдает выражения, у которых левый аргумент часто будет глубже правого. 
    - Поэтому:
        1. Вызывается функция [translate_to_value_arg()](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/csa_lab3/translator/expressions.py#L115) для правого аргумента. 
            - Если у него есть immediate value или адрес, то возвращается просто аргумент с видом адресации
            - Если ничего нет, то:
                1. Он транслируется в аккумулятор(генерируется последовательность команд, после выполнения которых результат выражения окажется в аккумуляторе) 
                2. в трансляторе выделяется временная переменная 
                3. значение складывается в переменную
        2. Вызывается функция translate() для левого аргумента - левый аргумент транслируется в аккумулятор 
        3. Применяется необходимая операция, в аргументах которой подставляется значение правого аргумента(с нужной адресацией).

Вся эта логика реализована [здесь](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/csa_lab3/translator/expressions.py#L368).

Пример трансляции выражения: 
```
int b = a / 10 + 10 * (-5);:
  	22   LD   [IMMEDIATE] 5
  	23   NEG  
  	24   ST   [IMMEDIATE] 805306373
  	25   LD   [IMMEDIATE] 10
  	26   MUL  [DIRECT]    805306373
  	27   ST   [IMMEDIATE] 805306372
  	28   LD   [DIRECT]    805306370
  	29   DIV  [IMMEDIATE] 10
  	30   ADD  [DIRECT]    805306372
  	31   ST   [IMMEDIATE] 805306371
```

# Система команд

- Аккумуляторная архитектура, поэтому первым(левым) аргументом всегда выступает аккумулятор, второй(правый) аргумент опционален. Для второго аргумента предоставлены 3 вида адресации: непосредственная(immediate), прямая(direct) и косвенная(indirect). 

- Регистры доступные программисту помимо ACC отсутствуют. Взаимодействие с памятью происходит через аккумулятор и виды адресации. 

- Взаимодействие с вводом/выводом происходит посредством MMIO, поэтому для того чтобы считать следующий символ в аккумулятор можно воспользоваться командой LD с прямой адресацией по адресу устройства ввода. Также это позволяет использовать следующий символ в качестве аргумента в любой другой команде посредством прямой адресации. Для вывода достаточно воспользоваться командой ST с immediate value равным адресу устройства вывода.

- Счетчик команд указывает на команду, которая исполняется в данный момент, он автоматически изменяется на 1 во время цикла выборки. Программист может вмешаться в этот процесс посредством команд безусловного(JMP) и условного(JZ, JNZ) перехода, они позволяют изменить значение счетчика команд на значение своего аргумента. Напрямую нет возможности сделать JMP по значению аккумулятора, однако это можно провернуть, если сначала загрузить значение аккумулятора во временную ячейку памяти, а затем вызвать JMP с прямой адресацией по её адресу.

- Таблица команд:

| Оператор | Аргумент | Влияние на NZ | Описание |
| -------- | -------- | ------------- | -------- |
| LD       |     +    |       +       | Загружает аргумент в аккумулятор |       
| ST       |     +    |       -       | Загружает значение аргумента в ячейку памяти с адресом равным аргументу |
| ADD      |     +    |       +       | Прибавить значение аргумента к аккумулятору |
| SUB      |     +    |       +       | Вычесть из аккумулятора значение аргумента |
| MUL      |     +    |       +       | Умножожить аккумулятор на значение аргумента |
| DIV      |     +    |       +       | Целочисленно разделить аккумулятор на аргумент |
| REM      |     +    |       +       | Записать в аккумулятор остаток от деления аккумулятора на аргумент |
| NEG      |     -    |       +       | Изменить знак аккумулятора (умножить на -1) |
| AND      |     +    |       +       | Применить к аккумулятору побитовое И с аргументом |
| OR       |     +    |       +       | Применить к аккумулятору побитовое ИЛИ с аргументом |
| NOT      |     -    |       +       | Применить логические отрицание к аккумулятору(если в нем был 0, то станет 1, если был не 0, то станет 0) |
| CMP      |     +    |       +       | Проставить флаги NZ как при операции ACC - ARG |
| SETG     |     -    |       +       | Если N=0 и Z=0 устанавливает 1 в аккумулятор, иначе 0 |
| SETE     |     -    |       +       | Если N=0 и Z=1 устанавливает 1 в аккумулятор, иначе 0 |
| SETL     |     -    |       +       | Если N=1 и Z=0 устанавливает 1 в аккумулятор, иначе 0 |
| JMP      |     +    |       -       | Безусловный переход на инструкцию с номером равным значению аргумента |
| JZ       |     +    |       -       | Переход на инструкцию с номером равным значению аргумента, если установлен флаг Z |
| JNZ      |     +    |       -       | Переход на инструкцию с номером равным значению аргумента, если не установлен флаг Z |
| HALT     |     -    |       -       | Окончание симуляции |

- Кодирование инструкций при помощи высокоуровневой структуры: у каждой команды есть номер, opcode и возможно аргумент с видом адресации, это всё записывается через пробельные символы в виде текста. Пример:
```
38   ADD  [DIRECT]    805306373
39   ADD  [IMMEDIATE] 1
40   ST   [IMMEDIATE] 805306374
41   LD   [INDIRECT]  805306374
```

# Транслятор

```
python3 csa_lab3/translator/main.py CODE_FILE OUTPUT_FILE [-v]
```
- Транслятор принимает первым аргумент входной файл с кодом. В качестве второго аргумента имя выходного файла. По умолчанию транслятор выводит просто инструкции друг за другом в вышеописанном формате. Если добавить флаг -v в качестве 3 аргумента, то транслятор будет выводить строки исходного кода соотвествующие командам процессора. \
Пример вывода с -v:
```
print('o');:
    45   LD   [IMMEDIATE] 111
    46   ST   [IMMEDIATE] 268435457
print('k');:
    47   LD   [IMMEDIATE] 107
    48   ST   [IMMEDIATE] 268435457
if (a < b) {:
    49   LD   [DIRECT]    805306370
    50   CMP  [DIRECT]    805306371
    51   SETL 
    52   JZ   [IMMEDIATE] 55
```

- Транслятор сначала парсит исходный код в список высказываний(statements), каждое высказывание представляет из себя узел AST, который имеет внутри какое то количество потомков. Затем осуществляется трансляция путем вызова у каждого высказывания метода `translate()`, внутри которого узел вызывает такой же метод у своих детей. Во время разбиения на токены, в них записывается информация о номере строки, из которой они были считаны. Во время трансляции эта информация переходит в инструкции и используется для подробного вывода. [Пример метода translate](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/csa_lab3/translator/statements.py#L291)

- Токены перечисляются в [этом enum](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/csa_lab3/translator/tokenizer.py#L75). Токены могут быть заданы либо строкой, тогда функция генерируется автоматически, либо функцией, которая их парсит.

- Парсер представляет из себя самодельный LL(1) парсер. Это означает, что он в [основном цикле](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/csa_lab3/translator/statements.py#L44) по очереди перебирает подходящие по контексту правила и применяет первое успешное. Для определения того подходит правило или нет необходимо и достаточно посмотреть 1 токен. Парсер работает за один проход, то есть потоково превращает код в токены, а токены в узлы. [(Пример правила)](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/csa_lab3/translator/statements.py#L175) 

- Результатом работы транслятора является список инструкций, однако методы `translate()`, которые используются для трансляции узлов вовзращают не только их. Помимо инструкций они возвращают метки и помеченные инструкции. Метки представляют из себя просто обьект, содержащий единственное поле - номер строки. Помеченные инструкции являются обьединением ссылки на метку и инструкции. После того как все методы отработали, транслятор по очереди схлопывает метки: устанавливает в поле значение текущей строки и удаляет метку из списка. Затем транслятор второй раз проходит по списку и схлопывает помеченные инструкции: устанавливает в качестве аргумента инструкции номер строки метки и превращает инструкцию в обычную. [Эта логика реализована здесь](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/csa_lab3/translator/translator.py#L21)

- Таким образом, при помощи меток и помеченных инструкций, транслируются if_stmt и while_stmt. 

- Особого внимания так же стоит трансляция выражений со скобками. Если оно встречается слева от оператора присваивания, то адрес нужного символа высчитывается в аккумулятор и загружается во временную ячейку, затем после трансляции выражения справа от скобок вызывается ST с прямой адресацией по адресу временной ячейки. Если выражение встречается в качестве операнда, то адрес нужного символа так же высчитывается в аккумулятор и загружается во временную ячейку, затем вызывается LD с косвенной адресацией по адресу временной ячейки. [Первый случай](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/csa_lab3/translator/expressions.py#L177). [Второй случай](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/csa_lab3/translator/expressions.py#L323)

- Процесс высчитывания адреса нужного символа в ячейку прост: к значению выражения в скобках просто прибавляется известный адрес строки и 1, потому что в первой ячейке строки находится её длина.

- При трансляции output_stmt раскрывается inline в зависимости от типа выражения, при `char` это просто `ST` в адрес устройства, при `string` это цикл, который по очереди выводит все символы из данной строки на основе информации о её длине(pstr). [Реализация](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/csa_lab3/translator/statements.py#L193)

# Модель процессора
```
python3 csa_lab3/machine/main.py CODE_FILE INPUT_FILE [LOG_FILE]
```

- Модель процессора первым аргументом принимает файл с машинными инструкциями(можно в перемешку с исходным кодом, вывод транслятора с флагом -v). Вторым аргументом принимает файл с входными данными для устройства ввода, и третьим опциональным аргументом принимает файл для журнала событий. В стандартный вывод модель выводит содержимое устройства вывода по окончании моделирования.

- Моделирование осуществляется потактово, до тех пор пока не возникнет исключение `StopIteration`, которое выбрасывается при выполнении HALT или при израсходовании входного буфера. 

- У каждый инструкции 3 стадии выполнения: цикл выборки инструкции, цикл выборки операнда и цикл исполнения. Instruction decoder запоминает текущую стадию и текущий шаг, если выполнения цикла занимает более одного такта. При вызове метода [tick()](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/csa_lab3/machine/control_unit.py#L200), вызывается соответствующая текущей стадии функция и счетчик тактов увеличивается на 1.

- [Цикл выборки инструкции](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/csa_lab3/machine/control_unit.py#L68): защелкивается значения счетчика инструкций(оно зависит от прошлой команды, в частности от сигнала на мультиплексоре sel_next), читается новая команда и на мультиплексоре выбора следующей инструкции устанавливается значение инкремента(то есть если команда ничего не изменит следующей командой будет следующая по счету). Также на этом этапе сбрасываются сигналы OE, WR и ACC_OUT для Datapath и на мультиплексоре выбора аргумента устанавливается значение непосредственной адресации. Цикл всегда длится 1 такт.

- [Цикл выборки оператора](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/csa_lab3/machine/control_unit.py#L83): может быть пропущен если аргумент отсутствует или используется непосредственная адресация. Если используется прямая адресация, то значение аргумента устанавливается в значении адреса(защелкивается) и выставляется сигнал OE, цикл занимает 1 такт. Если используется косвенная адресация, то действия повторяются еще один раз и цикл занимает 2 такта.

- [Цикл исполнения](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/csa_lab3/machine/control_unit.py#L117): на этом этапе на линии аргумента находится значение аргумента команды. Во время него происходит непосредственное исполнения. На этом этапе, посылаются сигналы для АЛУ, аргументы проходят через него и защелкивается новое значение аккумулятора. Может происходить запись в память(команда ST). Также во время этого цикла устанавливаются флаги NZ. Этот цикл всегда длится 1 такт.

Схемы:\
[control unit](control_unit.png) \
[data path](datapath.png)

# Тестирование

Тесты:
1. [cat](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/golden/cat.yml)
2. [hello world](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/golden/hello.yml)
3. [hello user name](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/golden/hello_user_name.yml)
4. [prob2](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/golden/prob2.yml) - сумма всех четных чисел Фибоначчи до N
5. [simple](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/golden/simple.yml) - тест, который проверяет сложные выражения и разные синтаксические конструкции языка.

Для каждого теста выполняются 3 теста:
1. [test_tokenizer](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/tests/test_translator.py#L17) - проверяет процесс токенизации, сверяет в голден тестах вывод в виде токенов
2. [test_parser](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/tests/test_translator.py#L29) - проверяет процесс парсинга, сверяет в голден тестах вывод в виде списка узлов AST
3. [test_translator_and_machine](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/tests/test_translator.py#L45) - интеграционный тест

CI при помощи gitlab:

``` yaml
lab3-example:
  stage: test
  image:
    name: ryukzak/python-tools
    entrypoint: [""]
  script:
    - poetry install
    - coverage run -m pytest --verbose
    - find . -type f -name "*.py" | xargs -t coverage report
    - ruff format --check .
    - ruff check .
```

где:

- `poetry` -- управления зависимостями для языка программирования Python.
- `coverage` -- формирование отчёта об уровне покрытия исходного кода.
- `pytest` -- утилита для запуска тестов.
- `ruff` -- утилита для форматирования и проверки стиля кодирования.

[pyproject.toml](https://gitlab.se.ifmo.ru/malevrovich/csa-lab3/-/blob/master/pyproject.toml) - настройки зависимостей

Пример использования на примере программы cat:
``` powershell
(.venv) PS D:\Programming\csa\csa-lab3> cat .\programs\cat
char c = getc(); 
while (c != '\n')
{
  print(c);      
  c = getc();    
}
(.venv) PS D:\Programming\csa\csa-lab3> python .\csa_lab3\translator\main.py programs\cat programs\cat.isa -v
(.venv) PS D:\Programming\csa\csa-lab3> cat .\programs\cat.isa  
char c = getc();:
        0    LD   [DIRECT]    268435456
        1    ST   [IMMEDIATE] 805306368
while (c != '\n'):
        2    LD   [DIRECT]    805306368
        3    CMP  [IMMEDIATE] 10
        4    SETE 
        5    NOT
        6    JZ   [IMMEDIATE] 12
print(c);:
        7    LD   [DIRECT]    805306368
        8    ST   [IMMEDIATE] 268435457
c = getc();:
        9    LD   [DIRECT]    268435456
        10   ST   [IMMEDIATE] 805306368
while (c != '\n'):
        11   JMP  [IMMEDIATE] 2
No line:
        12   HALT
(.venv) PS D:\Programming\csa\csa-lab3> cat .\programs\cat.input
foo
(.venv) PS D:\Programming\csa\csa-lab3> python .\csa_lab3\machine\main.py programs\cat.isa programs\cat.input programs\cat.log
foo
```

Пример лога:
```
TICK:     0 STAGE: INSTR_FETCH ACC:          0 PC:  -1 DATA_ADDR:          0 ARG:          0 DATA_BUS:          0 N|Z: 0|1

Fetched: LD   [DIRECT]    268435456
TICK:     1 STAGE: ARG_FETCH   ACC:          0 PC:   0 DATA_ADDR:          0 ARG:  268435456 DATA_BUS:          0 N|Z: 0|1
input: ['\n', 'o', 'o'] --> 'f'
TICK:     2 STAGE: EXECUTION   ACC:          0 PC:   0 DATA_ADDR:  268435456 ARG:        102 DATA_BUS:        102 N|Z: 0|1
TICK:     3 STAGE: INSTR_FETCH ACC:        102 PC:   0 DATA_ADDR:  268435456 ARG:        102 DATA_BUS:        102 N|Z: 0|0

Fetched: ST   [IMMEDIATE] 805306368
TICK:     4 STAGE: ARG_FETCH   ACC:        102 PC:   1 DATA_ADDR:  268435456 ARG:  805306368 DATA_BUS:        102 N|Z: 0|0
Arg fetch stage skipped
TICK:     4 STAGE: EXECUTION   ACC:        102 PC:   1 DATA_ADDR:  268435456 ARG:  805306368 DATA_BUS:        102 N|Z: 0|0
TICK:     5 STAGE: INSTR_FETCH ACC:        102 PC:   1 DATA_ADDR:  805306368 ARG:  805306368 DATA_BUS:        102 N|Z: 0|0

Fetched: LD   [DIRECT]    805306368
TICK:     6 STAGE: ARG_FETCH   ACC:        102 PC:   2 DATA_ADDR:  805306368 ARG:  805306368 DATA_BUS:        102 N|Z: 0|0
TICK:     7 STAGE: EXECUTION   ACC:        102 PC:   2 DATA_ADDR:  805306368 ARG:        102 DATA_BUS:        102 N|Z: 0|0
TICK:     8 STAGE: INSTR_FETCH ACC:        102 PC:   2 DATA_ADDR:  805306368 ARG:        102 DATA_BUS:        102 N|Z: 0|0

Fetched: CMP  [IMMEDIATE] 10
TICK:     9 STAGE: ARG_FETCH   ACC:        102 PC:   3 DATA_ADDR:  805306368 ARG:         10 DATA_BUS:        102 N|Z: 0|0
Arg fetch stage skipped
TICK:     9 STAGE: EXECUTION   ACC:        102 PC:   3 DATA_ADDR:  805306368 ARG:         10 DATA_BUS:        102 N|Z: 0|0
TICK:    10 STAGE: INSTR_FETCH ACC:        102 PC:   3 DATA_ADDR:  805306368 ARG:         10 DATA_BUS:        102 N|Z: 0|0

Fetched: SETE 
TICK:    11 STAGE: ARG_FETCH   ACC:        102 PC:   4 DATA_ADDR:  805306368 ARG:          0 DATA_BUS:        102 N|Z: 0|0
Arg fetch stage skipped
TICK:    11 STAGE: EXECUTION   ACC:        102 PC:   4 DATA_ADDR:  805306368 ARG:          0 DATA_BUS:        102 N|Z: 0|0
TICK:    12 STAGE: INSTR_FETCH ACC:          0 PC:   4 DATA_ADDR:  805306368 ARG:          0 DATA_BUS:        102 N|Z: 0|1

Fetched: NOT  
TICK:    13 STAGE: ARG_FETCH   ACC:          0 PC:   5 DATA_ADDR:  805306368 ARG:          0 DATA_BUS:        102 N|Z: 0|1
Arg fetch stage skipped
TICK:    13 STAGE: EXECUTION   ACC:          0 PC:   5 DATA_ADDR:  805306368 ARG:          0 DATA_BUS:        102 N|Z: 0|1
TICK:    14 STAGE: INSTR_FETCH ACC:          1 PC:   5 DATA_ADDR:  805306368 ARG:          0 DATA_BUS:        102 N|Z: 0|0

Fetched: JZ   [IMMEDIATE] 12
TICK:    15 STAGE: ARG_FETCH   ACC:          1 PC:   6 DATA_ADDR:  805306368 ARG:         12 DATA_BUS:        102 N|Z: 0|0
Arg fetch stage skipped
TICK:    15 STAGE: EXECUTION   ACC:          1 PC:   6 DATA_ADDR:  805306368 ARG:         12 DATA_BUS:        102 N|Z: 0|0
TICK:    16 STAGE: INSTR_FETCH ACC:          1 PC:   6 DATA_ADDR:  805306368 ARG:         12 DATA_BUS:        102 N|Z: 0|0

Fetched: LD   [DIRECT]    805306368
TICK:    17 STAGE: ARG_FETCH   ACC:          1 PC:   7 DATA_ADDR:  805306368 ARG:  805306368 DATA_BUS:        102 N|Z: 0|0
TICK:    18 STAGE: EXECUTION   ACC:          1 PC:   7 DATA_ADDR:  805306368 ARG:        102 DATA_BUS:        102 N|Z: 0|0
TICK:    19 STAGE: INSTR_FETCH ACC:        102 PC:   7 DATA_ADDR:  805306368 ARG:        102 DATA_BUS:        102 N|Z: 0|0

Fetched: ST   [IMMEDIATE] 268435457
TICK:    20 STAGE: ARG_FETCH   ACC:        102 PC:   8 DATA_ADDR:  805306368 ARG:  268435457 DATA_BUS:        102 N|Z: 0|0
Arg fetch stage skipped
TICK:    20 STAGE: EXECUTION   ACC:        102 PC:   8 DATA_ADDR:  805306368 ARG:  268435457 DATA_BUS:        102 N|Z: 0|0
output: [] <-- 'f'
TICK:    21 STAGE: INSTR_FETCH ACC:        102 PC:   8 DATA_ADDR:  268435457 ARG:  268435457 DATA_BUS:        102 N|Z: 0|0

... Тут происходит чтение и вывод еще двух символов 'o'

Fetched: LD   [DIRECT]    268435456
TICK:    68 STAGE: ARG_FETCH   ACC:        111 PC:   9 DATA_ADDR:  268435457 ARG:  268435456 DATA_BUS:        111 N|Z: 0|0
input: [] --> '
'
TICK:    69 STAGE: EXECUTION   ACC:        111 PC:   9 DATA_ADDR:  268435456 ARG:         10 DATA_BUS:         10 N|Z: 0|0
TICK:    70 STAGE: INSTR_FETCH ACC:         10 PC:   9 DATA_ADDR:  268435456 ARG:         10 DATA_BUS:         10 N|Z: 0|0

Fetched: ST   [IMMEDIATE] 805306368
TICK:    71 STAGE: ARG_FETCH   ACC:         10 PC:  10 DATA_ADDR:  268435456 ARG:  805306368 DATA_BUS:         10 N|Z: 0|0
Arg fetch stage skipped
TICK:    71 STAGE: EXECUTION   ACC:         10 PC:  10 DATA_ADDR:  268435456 ARG:  805306368 DATA_BUS:         10 N|Z: 0|0
TICK:    72 STAGE: INSTR_FETCH ACC:         10 PC:  10 DATA_ADDR:  805306368 ARG:  805306368 DATA_BUS:         10 N|Z: 0|0

Fetched: JMP  [IMMEDIATE] 2
TICK:    73 STAGE: ARG_FETCH   ACC:         10 PC:  11 DATA_ADDR:  805306368 ARG:          2 DATA_BUS:         10 N|Z: 0|0
Arg fetch stage skipped
TICK:    73 STAGE: EXECUTION   ACC:         10 PC:  11 DATA_ADDR:  805306368 ARG:          2 DATA_BUS:         10 N|Z: 0|0
TICK:    74 STAGE: INSTR_FETCH ACC:         10 PC:  11 DATA_ADDR:  805306368 ARG:          2 DATA_BUS:         10 N|Z: 0|0

Fetched: LD   [DIRECT]    805306368
TICK:    75 STAGE: ARG_FETCH   ACC:         10 PC:   2 DATA_ADDR:  805306368 ARG:  805306368 DATA_BUS:         10 N|Z: 0|0
TICK:    76 STAGE: EXECUTION   ACC:         10 PC:   2 DATA_ADDR:  805306368 ARG:         10 DATA_BUS:         10 N|Z: 0|0
TICK:    77 STAGE: INSTR_FETCH ACC:         10 PC:   2 DATA_ADDR:  805306368 ARG:         10 DATA_BUS:         10 N|Z: 0|0

Fetched: CMP  [IMMEDIATE] 10
TICK:    78 STAGE: ARG_FETCH   ACC:         10 PC:   3 DATA_ADDR:  805306368 ARG:         10 DATA_BUS:         10 N|Z: 0|0
Arg fetch stage skipped
TICK:    78 STAGE: EXECUTION   ACC:         10 PC:   3 DATA_ADDR:  805306368 ARG:         10 DATA_BUS:         10 N|Z: 0|0
TICK:    79 STAGE: INSTR_FETCH ACC:         10 PC:   3 DATA_ADDR:  805306368 ARG:         10 DATA_BUS:         10 N|Z: 0|1

Fetched: SETE 
TICK:    80 STAGE: ARG_FETCH   ACC:         10 PC:   4 DATA_ADDR:  805306368 ARG:          0 DATA_BUS:         10 N|Z: 0|1
Arg fetch stage skipped
TICK:    80 STAGE: EXECUTION   ACC:         10 PC:   4 DATA_ADDR:  805306368 ARG:          0 DATA_BUS:         10 N|Z: 0|1
TICK:    81 STAGE: INSTR_FETCH ACC:          1 PC:   4 DATA_ADDR:  805306368 ARG:          0 DATA_BUS:         10 N|Z: 0|0

Fetched: NOT  
TICK:    82 STAGE: ARG_FETCH   ACC:          1 PC:   5 DATA_ADDR:  805306368 ARG:          0 DATA_BUS:         10 N|Z: 0|0
Arg fetch stage skipped
TICK:    82 STAGE: EXECUTION   ACC:          1 PC:   5 DATA_ADDR:  805306368 ARG:          0 DATA_BUS:         10 N|Z: 0|0
TICK:    83 STAGE: INSTR_FETCH ACC:          0 PC:   5 DATA_ADDR:  805306368 ARG:          0 DATA_BUS:         10 N|Z: 0|1

Fetched: JZ   [IMMEDIATE] 12
TICK:    84 STAGE: ARG_FETCH   ACC:          0 PC:   6 DATA_ADDR:  805306368 ARG:         12 DATA_BUS:         10 N|Z: 0|1
Arg fetch stage skipped
TICK:    84 STAGE: EXECUTION   ACC:          0 PC:   6 DATA_ADDR:  805306368 ARG:         12 DATA_BUS:         10 N|Z: 0|1
TICK:    85 STAGE: INSTR_FETCH ACC:          0 PC:   6 DATA_ADDR:  805306368 ARG:         12 DATA_BUS:         10 N|Z: 0|1

Fetched: HALT 
TICK:    86 STAGE: ARG_FETCH   ACC:          0 PC:  12 DATA_ADDR:  805306368 ARG:          0 DATA_BUS:         10 N|Z: 0|1
Arg fetch stage skipped
TICK:    86 STAGE: EXECUTION   ACC:          0 PC:  12 DATA_ADDR:  805306368 ARG:          0 DATA_BUS:         10 N|Z: 0|1
```

Пример проверки исходного кода: 
``` powershell
(.venv) PS D:\Programming\csa\csa-lab3> poetry run pytest --update-goldens -v
=================================================================== test session starts ====================================================================
platform win32 -- Python 3.11.0, pytest-7.4.3, pluggy-1.3.0 -- D:\Programming\csa\csa-lab3\.venv\Scripts\python.exe
cachedir: .pytest_cache
rootdir: D:\Programming\csa\csa-lab3
configfile: pyproject.toml
testpaths: tests
plugins: golden-0.2.2
collected 15 items

tests/test_translator.py::test_tokenizer[../golden/cat.yml] PASSED                                                                                    [  6%]
tests/test_translator.py::test_tokenizer[../golden/hello.yml] PASSED                                                                                  [ 13%]
tests/test_translator.py::test_tokenizer[../golden/hello_user_name.yml] PASSED                                                                        [ 20%]
tests/test_translator.py::test_tokenizer[../golden/prob2.yml] PASSED                                                                                  [ 26%]
tests/test_translator.py::test_tokenizer[../golden/simple.yml] PASSED                                                                                 [ 33%]
tests/test_translator.py::test_parser[../golden/cat.yml] PASSED                                                                                       [ 40%]
tests/test_translator.py::test_parser[../golden/hello.yml] PASSED                                                                                     [ 46%]
tests/test_translator.py::test_parser[../golden/hello_user_name.yml] PASSED                                                                           [ 53%]
tests/test_translator.py::test_parser[../golden/prob2.yml] PASSED                                                                                     [ 60%]
tests/test_translator.py::test_parser[../golden/simple.yml] PASSED                                                                                    [ 66%]
tests/test_translator.py::test_translator_and_machine[../golden/cat.yml] PASSED                                                                       [ 73%]
tests/test_translator.py::test_translator_and_machine[../golden/hello.yml] PASSED                                                                     [ 80%]
tests/test_translator.py::test_translator_and_machine[../golden/hello_user_name.yml] PASSED                                                           [ 86%]
tests/test_translator.py::test_translator_and_machine[../golden/prob2.yml] PASSED                                                                     [ 93%]
tests/test_translator.py::test_translator_and_machine[../golden/simple.yml] PASSED                                                                    [100%]
============================================================= 15 passed, 15 warnings in 8.88s ============================================================== 
(.venv) PS D:\Programming\csa\csa-lab3> poetry run ruff check .
(.venv) PS D:\Programming\csa\csa-lab3> poetry run ruff format .
16 files left unchanged
```

Статистика:
```
| ФИО | <алг> | <LoC> | <code байт> | <code инстр.> | <инстр.> | <такт.> | <вариант> |
| Скрябин Иван Александрович | cat | 5 | - | 12 | 38 | 86 | alg | acc | harv | hw | tick | struct | stream | mem | pstr | prob2 | [4]char |
| Скрябин Иван Александрович | hello | 1 | - | 42 | 175 | 442 | alg | acc | harv | hw | tick | struct | stream | mem | pstr | prob2 | [4]char |
| Скрябин Иван Александрович | hello_user_name | 17 | - | 131 | 728 | 1820 | alg | acc | harv | hw | tick | struct | stream | mem | pstr | prob2 | [4]char |
