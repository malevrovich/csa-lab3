from __future__ import annotations

from csa_lab3.isa import WORD_SIZE, ArgType, Instruction, Opcode
from csa_lab3.translator.common import TranslateError


class Mark:
    def __init__(self) -> None:
        self.position = None

    def set_position(self, pos) -> None:
        self.position = pos


class MarkedInstr:
    def __init__(self, instr: Instruction, mark: Mark) -> None:
        self.instr = instr
        self.mark = mark


def resolve_marks(instrs: list[Instruction | Mark | MarkedInstr]) -> list[Instruction]:
    mark_cnt = 0
    for pos, i in enumerate(instrs):
        if isinstance(i, Mark):
            i.set_position(pos - mark_cnt)
            mark_cnt += 1

    res: list[Instruction] = []
    for r in filter(lambda x: not isinstance(x, Mark), instrs):
        if isinstance(r, MarkedInstr):
            r.instr.arg = r.mark.position
            res.append(r.instr)
        if isinstance(r, Instruction):
            res.append(r)

    return res  # type: ignore


class Translator:
    def __init__(self) -> None:
        self.input_addr = 0x10000000
        self.output_addr = 0x10000001

        self.data_start = 0x20000000
        self.data_end = 0x2FFFFFFF
        self.free_data = self.data_start

        self.mem_start = 0x30000000
        self.mem_end = 0x3FFFFFFF
        self.mem_cur = 0x30000000
        self.mem_free_list: list[int] = []

        self.preload: list[Instruction] = []

        self.var_table: dict[str, int] = {}

    def allocate_data(self, size: int, val: list[int]) -> int:
        assert size >= len(val)

        if self.free_data + size >= self.data_end:
            raise TranslateError(message="Data memory exhausted")
        for i, v in enumerate(val):
            if v < 0 or v >= 2 ** (WORD_SIZE):
                raise TranslateError(message="Store immediate argument bigger than word")
            self.preload.append(Instruction(Opcode.LD, v, ArgType.IMMEDIATE))
            self.preload.append(Instruction(Opcode.ST, self.free_data + i, ArgType.IMMEDIATE))
        res = self.free_data
        self.free_data += size
        return res

    def _allocate_prog_mem(self) -> int:
        if self.mem_free_list:
            return self.mem_free_list.pop()
        if self.mem_cur < self.mem_end:
            self.mem_cur += 1
            return self.mem_cur - 1
        raise TranslateError(message="Program memory exhausted")

    def _free_prog_mem(self, ptr: int) -> None:
        self.mem_free_list.append(ptr)

    def allocate_for_tmp_expr(self) -> int:
        return self._allocate_prog_mem()

    def free_tmp_expr(self, ptr: int) -> None:
        self._free_prog_mem(ptr)

    def allocate_var(self, name: str) -> int:
        addr = self._allocate_prog_mem()
        self.var_table[name] = addr
        return addr

    def get_var_addr(self, name: str) -> int:
        return self.var_table[name]
