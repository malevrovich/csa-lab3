from __future__ import annotations

import enum

from csa_lab3.translator.tokenizer import Token, TokenType


@enum.unique
class AstExprType(enum.IntFlag):
    INT = enum.auto()
    CHAR = enum.auto()
    STRING = enum.auto()


class AstVariable:
    def __init__(self, var_type: AstExprType, name: str) -> None:
        self.type = var_type
        self.name = name


class ParseError(Exception):
    "Exception raised for errors during translation"

    def __init__(
        self, token: Token | None = None, expected: TokenType | None = None, message: str | None = None
    ) -> None:
        if message is not None:
            super().__init__(message)
            return
        if token is None and expected is not None:
            super().__init__(f"Failed to parse: no tokens left, expected {expected.name}")
            return
        if token is not None and expected is not None:
            super().__init__(f"Failed to parse: wrong token {token.token_type.name} expected {expected.name}")
            return
        if token is not None and expected is None:
            super().__init__(f"Failed to parse: wrong token {token.token_type.name}")
            return

        super().__init__("Unknown parse error")


class TranslateError(Exception):
    def __init__(self, message: str) -> None:
        super().__init__(f"TranslateError occured: {message}")
