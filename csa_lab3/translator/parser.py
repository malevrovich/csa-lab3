from __future__ import annotations

from io import TextIOBase

from csa_lab3.translator.common import AstVariable
from csa_lab3.translator.statements import AstStmt
from csa_lab3.translator.tokenizer import Tokenizer


class Parser:
    def __init__(self, text: TextIOBase) -> None:
        self.statements: list[AstStmt] = []
        self.tokenizer = Tokenizer(text)
        self.sym_table: dict[str, AstVariable] = dict()

    def parse(self) -> None:
        while self.get_next_stmt():
            pass

    def get_next_stmt(self) -> AstStmt | None:
        stmt = AstStmt.parse(self.tokenizer, self.sym_table)
        if stmt:
            self.statements.append(stmt)
        return stmt
